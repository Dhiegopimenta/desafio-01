package br.com.itau;

import java.util.List;

import br.com.itau.modelo.Lancamento;
import br.com.itau.service.LancamentoService;

public class Exercicio {

	public static void main(String[] args) {
	List<Lancamento> lancamentos = new LancamentoService().listarLancamentos();
        System.out.println("Organizando a lista de forma ordenada pelo ID");
        System.out.print("=============================================");
        System.out.println("\n");
        for (Lancamento lancamento : lancamentos) {
            System.out.println("ID: " + lancamento.getId() + " Valor: " + lancamento.getValor() + " Origem :"
                    + lancamento.getOrigem() + " Categoria: " + lancamento.getCategoria() + " Mês: "
                    + lancamento.getMes());
        }

        System.out.println("Gastos ordenados por mês");
        System.out.print("=============================================");
        System.out.println("\n");
        List<Lancamento> porMes = new LancamentoService().listarLancamentos();
        Map<Integer, List<Lancamento>> map = new HashMap<Integer, List<Lancamento>>();
        for (Lancamento gasto : porMes) {
            Integer key = gasto.getMes();
            if (map.get(key) == null) {
                map.put(key, new ArrayList<Lancamento>());
            }
            map.get(key).add(gasto);
        }

        map.forEach((id, mes) -> {
            System.out.println("Mês : " + id + " Fatura : " + mes);
        });

        System.out.println("Lançamento pela categoria");
        System.out.print("=============================================");
        System.out.println("\n");
        for (Lancamento lancamento : lancamentos) {
            if (lancamento.getCategoria() == 1) {
                System.out.println("ID: " + lancamento.getId() + " Valor: " + lancamento.getValor() + " Origem :"
                        + lancamento.getOrigem() + " Categoria: " + lancamento.getCategoria() + " Mês: "
                        + lancamento.getMes());
            }
        }

        System.out.println("Total da fatura de um determinado mês");
        System.out.print("=============================================");
        System.out.println("\n");
        Double soma = 0.0;
        for (Lancamento lancamento : lancamentos) {
            if (lancamento.getMes() == 1) {
                soma += lancamento.getValor();
            }
        }
        System.out.println("Soma do mês de janeiro: " + soma);
	}

}
